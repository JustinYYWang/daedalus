/** @type {import('next').NextConfig} */
const nextConfig = {
    experimental: {
        serverActions: {
        // edit: updated to new key. Was previously `allowedForwardedHosts`
        allowedOrigins: ['robg.sistinespectrum.adaward.com'],
        },
  },
};

module.exports = nextConfig;
