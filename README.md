# Daedalus

![image](https://gitlab.com/JustinYYWang/daedalus/-/raw/main/Public/Structure.png?inline=false)

## Description
Daedalus整合各個 Open Source Software，主要服務 Sistine Spectrum 專案開發、桌遊網站，與整合 QNAP NAS
架構由 Raspberry Pi 5 以 Docker 容器化方式建構服務平台，資料則存放於 NAS
由 Nginx 作為反向代理服務器，將各網址導到不同服務站，分別是：

1. **遊戲追蹤器** Game Tracker, Next.js 自架網站 :: https://gtracker.sistinespectrum.adaward.com (開發中) 
    - 已利用 Next.js 官網的教學模板架出雛型、更改其中的程式庫並連線至自己指定的 PostgreSQL DB 待網站設計完成繼續開發與 Notion 資料移轉

2. **維基百科** wiki, 開源套件 wiki.js :: ~ (尚未開始)

3. **專案管理平台** Projects, 開源套件OpenProject :: ~ (尚未開始)

4. **資料儲存區** Qnap NAS 與其上的 PostgreSQL DB :: https://spencerts451.sistinespectrum.adaward.com
    - 利用 Qnap NAS 並在其上開容器建 PostgreSQL DB 和 pgAdmin

5. **桌遊平台** Republic of Board Games, Next.js 自架網站 :: https://robg.sistinespectrum.adaward.com (開發中)
    - 已利用 Next.js 官網的教學模板架出雛型、更改其中的程式庫並連線至自己指定的 PostgreSQL DB 待網站設計完成繼續開發與 Notion 資料移轉

RoBG, GTracker 使用者密碼為：
user@nextmail.com
123456

## Installation
整個專案都是使用 Docker ，目前不需要任何前置安裝（整個專案應該在 Docker 啟動時就將環境準備好）

## Usage
進入目錄 /daedalus/

- 建構整個專案

```
docker compose up -d
```

### Nginx

只建構Nginx的部分專案，通常是用來**除錯**或**開發**的
進入目錄 /daedalus/Nginx/

- 部署執行反向代理伺服器

```
docker compose up -d
```

- 執行並簽署 CA

```
docker-compose run --rm certbot certonly --webroot -w /var/www/certbot/ -d sistinespectrum.adaward.com
```

- 更新 CA

```
docker-compose run --rm certbot renew
```

- 重啟 Nginx

```
docker-compose exec webserver nginx -s reload
```

### RoBG

只建構 RoBG 的部分專案，通常是用來**除錯**或**開發**的
進入目錄 /daedalus/RoBG/

- 部署執行 RoBG

```
docker compose up -d
```

### GTracker

只建構 RoBG 的部分專案，通常是用來**除錯**或**開發**的
進入目錄 /daedalus/GTracker/

- 部署執行 GTracker

```
docker compose up -d
```

## Support

若在執行時遇到任何問題，請透過 e-mail 聯絡原作者（Justin Wang - justin17728@gmail.com）


## Roadmap

- [X] Nginx
    - [X] 架設與設定代理伺服器
    - [X] 產生 SSL/TLS 憑證(利用 CertBot)
- [X] Nas and Database
    - [X] 設定外部可存取 Qnap Nas
    - [X] 在其上開容器建 PostgreSQL DB 與 pgAdmin 作為其他專案的 DB
- [ ] RoBG
    - [X] 利用 Next.js 官網的教學模板架出雛型
    - [X] 更改其中的程式庫符合網站需求並設定連線至自己指定的 PostgreSQL DB
    - [ ] 依使用需求設計網站並開發
    - [ ] 從 Notion 做資料移轉至 PostgreSQL DB
- [ ] GTracker
    - [X] 利用 Next.js 官網的教學模板架出雛型
    - [X] 更改其中的程式庫符合網站需求並設定連線至自己指定的 PostgreSQL DB
    - [ ] 依使用需求設計網站並開發
    - [ ] 從Notion做資料移轉至 PostgreSQL DB
- [ ] Wiki
- [ ] Projects

## Contributing
這是一個私人專案，專為組織 Sistine Spectrum 與我個人(Justin Wang)使用。若任何與以上相關人士對此也有興趣請透過 e-mail 聯絡我（Justin Wang - justin17728@gmail.com），我們可以一起論如何使這個專案完善。
若想要了解建構這個所需的知識，建議可以從下列有極大幫助的網站開始：
1. [Docker](https://docker-curriculum.com/)
2. [Next.js](https://nextjs.org/learn)
3. [Nginx](https://www.nginx.com/resources/wiki/start/)
4. [Wiki.js](https://docs.requarks.io/)
5. [OpenProject](https://www.openproject.org/docs/getting-started/)

## Authors and acknowledgment
這個專案是由我（Justin Wang）主持與建構，特別感謝 Andy Wang 一同協助構建、構思與討論該專案。

## License
For open source projects, say how it is licensed.
