import { Metadata } from 'next';

export const metadata: Metadata = {
  title: 'customer',
  robots: { 
    index: false, 
    follow: false 
  }
};

export default function Page() {
    return <p>Customers Page</p>;
}